/*
 * main.cpp
 *
 *  Created on: 21-Jul-2020
 *      Author: sunbeam
 */


#include<stdio.h>
#include<iostream>
#include<iomanip>
#include<string>
#include"account.h"
#include"bank.h"
using namespace std;

int menulist(void)
{
	int choice;
	cout<<"0.Exit\n"<<endl;
	cout<<"1.Create Account\n"<<endl;
	cout<<"2.Deposit\n"<<endl;
	cout<<"3.Withdraw\n"<<endl;
	cout<<"4.Print Account Details\n"<<endl;
	cout<<"Enter choice\n"<<endl;
	cin>>choice;
	return choice;
}
int main(void )
{
	using namespace std;
	int choice,accnumber;
	float amount,balance;
	 Bank bank={-1};
	 Account account;

	while( (choice=menulist()))
	{
		switch (choice)
		{
		case 1:
			account.accept_account_info(&account);
			 accnumber=bank.create_account(&bank,account);
			account.print_account_number(accnumber);
			break;
		case 2:
			account.accept_account_number(&accnumber);
		account.accept_amount(&amount);
		balance =bank.deposit(&bank,accnumber,amount);
		account.print_balance(balance);
		break;
		case 3:
			account.accept_account_number(&accnumber);
			account.accept_amount(&amount);
		balance =bank.withdraw(&bank,accnumber,amount);
		account.print_balance(balance);
		break;
		case 4:
			account.accept_account_number(&accnumber);
		       account=account.getaccount_details(&bank,accnumber);
			    account.print_account_info(&account);
			    break;

		}
	}

	return 0;
}
